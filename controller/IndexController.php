<?php

namespace FuryBrains\Controller;

use Database\Entities;

class IndexController
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

//    /**
//     * @var \Doctrine\ORM\EntityManager
//     */
//    protected $entityManager;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
//        $this->entityManager = $entityManager;
    }

    public function newsAction(){
        global $em, $twig;
        $bbcodes = new \FuryBrains\Controller\AbstractController();
        $news_all = $em->getRepository(Entities\News::class)->findAll();
        // последние 3 новости для слайдера
        $news_last = $em->getRepository(Entities\News::class)->createQueryBuilder('a')
            ->select('a')
            ->setMaxResults(3)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();
        // выбор всех новостей, кроме тех, что попали в слайдер
        $news = $em->getRepository(Entities\News::class)->createQueryBuilder('a')
            ->select('a')
            ->setFirstResult(3)
            ->setMaxResults(10)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();
        foreach ($news as &$new) {
            $new->description = $bbcodes->replaceBBCode($new->description);
        }
        unset($new);
        foreach ($news_last as &$new) {
            $new->description = $bbcodes->replaceBBCode($new->description);
        }
        unset($new);
        // подгрузка шаблона и передача в него переменных
        echo $this->twig->render('index.twig', array(
            'news' => $news, // вернуть массив в обратном порядке
            'news_count' => count($news_all),
            'news_last' => $news_last
        ));
    }
    public function newsCategoryAction($k){
        $bbcodes = new \FuryBrains\Controller\AbstractController();
        global $em;
        $news_all = $em->getRepository(Entities\News::class)->findBy(array('category' => $k));
        $news = $em->getRepository(Entities\News::class)->createQueryBuilder('a')
            ->select('a')
            ->where("a.category = $k")
            ->orderBy('a.id', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
        foreach ($news as &$new) {
            $new->description = $bbcodes->replaceBBCode($new->description);
        }
        unset($new);
        echo $this->twig->render('news-category-page.twig', array(
            'news' => $news,
            'news_count' => count($news_all),
        ));
    }
    public function newsPostFullAction($k){
        $bbcodes = new \FuryBrains\Controller\AbstractController();
        global $em, $klein, $twig;
        $id =  intval(implode( $klein->request()->params(['id']) ));
        $comments = $em->getRepository(Entities\CommentsNews::class)->createQueryBuilder('a')
            ->select('a')
            ->where("a.post = $id")
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();
        $comments_number = count($comments);
        $news = $em->getRepository(Entities\News::class)->findBy(['id'=>$id]);
        $recommended_pos_2 = $em->getRepository(\Database\Entities\Recommended::class)->createQueryBuilder('a')
            ->select('a')
            ->where('a.position = 2')
            ->setMaxResults(3)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();
        if(isset($_SESSION['uid'])) {
            if ($em->getRepository(Entities\Rates::class)->findOneBy(
                array(
                    'post' => $id,
                    'user' => $_SESSION['uid'])
            )
            ) {
                $rates_rep = $em->getRepository(Entities\Rates::class)->findOneBy(
                    array(
                        'post' => $id,
                        'user' => $_SESSION['uid'])
                );
                if (isset($rates_rep)) {
                    $uniq_rate = intval($rates_rep->rate);
                }
            } else {
                $uniq_rate = 0;
            }
        }else{
            $uniq_rate = 0;
        }

        foreach ($news as &$new) {
            $new->description = $bbcodes->replaceBBCode($new->description);
        }
        unset($new);
        foreach ($news as &$new2) {
            $new2->text = $bbcodes->replaceBBCode($new2->text);
        }
        unset($new2);
        foreach ($comments as &$comment) {
            $comment->text = $bbcodes->replaceBBCode($comment->text);
        }
        unset($new2);
        echo $this->twig->render('news-post-full-page.twig', array(
            'news' => $news,
            'category_id' => $k,
            'comments' => $comments,
            'comments_number' => $comments_number,
            'uniq_rate' => $uniq_rate,
            'recommended_pos_2' => $recommended_pos_2
        ));
    }

    public function searchAction(){
        $bbcodes = new \FuryBrains\Controller\AbstractController();
        global $em;

        $page = $_GET['page']-1;
        if($page < 1){
            $page = 0;
        }
        $results = 10;

        $search = $_GET['s'];

        $news = $em->getRepository(Entities\News::class)->createQueryBuilder('s')
            ->select("s")
            ->setFirstResult($page*10)
            ->setMaxResults($results)
            ->where("s.title LIKE '%" . $search . "%'")
            ->orderBy('s.id', 'DESC')
            ->getQuery()
            ->getResult();
        $pages = intval(count($news)/$results);

        $news_count = count($news);
        foreach ($news as &$new) {
            $new->description = $bbcodes->replaceBBCode($new->description);
        }
        unset($new);
        echo $this->twig->render('search.twig', array(
            'news' => $news,
            'news_count' => $news_count,
            'search' => $search,
            'pages_number' => $pages+1,
            'current_page' => $page+1,
        ));
    }
    public function profileIndexAction(){
        if(isset($_SESSION['user_id'])) {
            global $em;
            $user = $em->getRepository(Entities\UsersAPIs::class)->findOneBy(array(
               'id' => $_SESSION['user_id'],
               'link' => $_SESSION['user_link']
            ));
            $user_comments = $em->getRepository(Entities\CommentsNews::class)->findBy(array(
                'user' => $_SESSION['user_id'],
            ));
            $user_likes = $em->getRepository(Entities\Rates::class)->findBy(array(
                'user' => $_SESSION['user_id'],
            ));
            echo $this->twig->render('profile.twig', array(
                'user' => $user,
                'user_comments' => $user_comments,
                'user_likes' => $user_likes,
                'user_comments_num' => count($user_comments),
                'user_likes_num' => count($user_likes)
            ));
        }else{
            echo $this->twig->render('errors/404.twig', array());
        }
    }
    public function developAction(){
        echo $this->twig->render('develop.twig', array());
    }
    public function aboutUsAction(){
        echo $this->twig->render('about-us.twig', array());
    }
    public function feedbackAction(){
        echo $this->twig->render('feedback.twig', array());
    }
}