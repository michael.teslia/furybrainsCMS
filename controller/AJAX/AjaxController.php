<?php

namespace FuryBrains\Controller;

use Database\Entities;

class AjaxController
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

//    /**
//     * @var \Doctrine\ORM\EntityManager
//     */
//    protected $entityManager;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
//        $this->entityManager = $entityManager;
    }

    public function commentSendAction()
    {
        global $em, $klein;
        $id =  intval(implode( $klein->request()->params(['id']) ));
        if(isset($_POST['comment'])) {

            $comment = htmlspecialchars($_POST['comment']);

            $comments = new Entities\CommentsNews();
            $comments->setPost($em->getRepository(Entities\News::class)->findOneBy(['id' => $id]));
            $comments->setUser($em->getRepository(Entities\UsersAPIs::class)->findOneBy(['id' => $_SESSION['user_id']]));
            $comments->setText($comment);
            $comments->setDate(date('d').'-'.date('m').'-'.date('Y').', '.date('g').':'.date('i').' '.date('a'));

            $em->persist($comments);
            $em->flush();

            $comments_repository = $em->getRepository(Entities\CommentsNews::class)->createQueryBuilder('a')
                ->select('a')
                ->where("a.post = $id")
                ->orderBy('a.id', 'DESC')
                ->getQuery()
                ->getResult();
            echo $this->twig->render('comments-loadup.twig', array(
                'comments' => $comments_repository
            ));
        }
    }
    public function rateAction()
    {
        global $em, $klein;
        $id =  intval(implode( $klein->request()->params(['id']) ));
        if(isset($_POST['rate'])) {
            $rate = htmlspecialchars($_POST['rate']);

            $rates_rep = $em->getRepository(Entities\Rates::class)->findOneBy(
                array('post' => $id, 'user' => $_SESSION['user_id'])
            );
            $rate_post = $em->getRepository(Entities\News::class)->findOneBy(['id' => $id]);
            $rate_user = $em->getRepository(Entities\UsersAPIs::class)->findOneBy(['id' => $_SESSION['user_id']]);
                $rates = new Entities\Rates();
                $rates->setId($rates_rep->id);
                $rates->setPost($rate_post);
                $rates->setUser($rate_user);
            if(intval($rates_rep->rate) == 1){
                $rates->setRate(0);
                $dec = $rate_post->rate - 1;
                $rate_post->setRate($dec);
                print_r($rate_post->rate);

            }else{
                $rates->setRate(1);
                $inc = $rate_post->rate + 1;
                $rate_post->setRate($inc);
                print_r($rate_post->rate);
            }
                $em->merge($rates);

            $em->flush();

            die();
        }
    }

    public function newsIndexScrollAction(){
        global $em;
        $max = $_POST['scroll'];
        // последние 3 новости для слайдера
        $news_last = $em->getRepository(Entities\News::class)->createQueryBuilder('a')
            ->select('a.id')
            ->setMaxResults(3)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();
        for($i=0;$i<3;$i++) {
            $slide_id[$i] = $news_last[$i]['id'];
        }
        $news = $em->getRepository(Entities\News::class)->createQueryBuilder('a')
            ->select('a')
            ->where("a.id != $slide_id[0]")
            ->andWhere("a.id != $slide_id[1]")
            ->andWhere("a.id != $slide_id[2]")
            ->orderBy('a.id', 'DESC')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
        echo $this->twig->render('news-category-loadup.twig', array(
            'news' => $news
        ));
    }

    public function newsCategoryScrollAction($k){
        global $em;
        $max = $_POST['scroll'];
        $news = $em->getRepository(Entities\News::class)->createQueryBuilder('a')
            ->select('a')
            ->where("a.category = $k")
            ->orderBy('a.id', 'DESC')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
        echo $this->twig->render('news-category-loadup.twig', array(
            'news' => $news
        ));
    }

    public function feedbackSendAction()
    {
        global $em;
        if(isset($_POST['name']) && $_POST['email'] && $_POST['message']) {

            $name = htmlspecialchars($_POST['name']);
            $email = htmlspecialchars($_POST['email']);
            $subject = htmlspecialchars($_POST['subject']);
            $message = htmlspecialchars($_POST['message']);

            $feedback = new Entities\Feedback();
            $feedback->setName($name);
            $feedback->setEmail($email);
            $feedback->setSubject($subject);
            $feedback->setMessage($message);

            $em->persist($feedback);
            $em->flush();

            print_r('Ваше сообщение оставлено!');
//            header('location:'.$_SERVER['HTTP_REFERER']);
            die();
        }
    }
}