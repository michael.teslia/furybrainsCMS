<?php
/**
 * Created by PhpStorm.
 * User: mikzt
 * Date: 12.02.2017
 * Time: 10:59
 */

namespace FuryBrains\Controller;

use Database\Entities;

class FilesController
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

//    /**
//     * @var \Doctrine\ORM\EntityManager
//     */
//    protected $entityManager;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
//        $this->entityManager = $entityManager;
    }

    public function filesIndexAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
            global $em;
            $page = $_GET['page']-1;
            if($page < 1){
                $page = 0;
            }
            $results = 10;

            $files_all = $em->getRepository(Entities\FileManager::class)->findAll();
            $pages = intval(count($files_all)/$results);

            $files = $em->getRepository(Entities\FileManager::class)->createQueryBuilder('s')
                ->select("s")
                ->setFirstResult($page*10)
                ->setMaxResults($results)
                ->orderBy('s.id', 'DESC')
                ->getQuery()
                ->getResult();
            echo $this->twig->render('admin/files-index.twig', array(
                'files' => $files,
                'pages_number' => $pages+1,
                'current_page' => $page+1
            ));
        }
        else {
                echo $this->twig->render('errors/403.twig');
            }
    }

    public function filesAddAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
        echo $this->twig->render('admin/files-add.twig');
        global $em, $base_url;
        if (isset($_POST['name'])) {
            $name = $_POST['name'];
            $type = $_POST['type'];

            $images =  array('gif','png' ,'jpg');
            $documents = array('pdf','doc','xls','docx','xlsx','rtf','psd','txt','zip','rar');
            $apps = array('exe','apk','ipa');
            $filename = $_FILES['file']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            if(in_array($ext,$images) ) {
                $folder = "upload/images/original/".date('Y').'/'.date('M').'/';
            }elseif (in_array($ext,$documents)){
                $folder = "upload/documents/".date('Y').'/'.date('M').'/';
            }elseif (in_array($ext,$apps)){
                $folder = "upload/apps/".date('Y').'/'.date('M').'/';
            }else{
                $folder = "upload/other/".date('Y').'/'.date('M').'/';
            }
            mkdir($folder, 0777, true);
            $path = $folder . date('d') .'_'. date('H') . '-' . date('i') .'-'. date('s') .'-'. $filename;
            move_uploaded_file($_FILES["file"]["tmp_name"], $path);

            if(in_array($ext,$images) ) {
                $folder_rec = "upload/images/rec/" . date('Y') . '/' . date('M') . '/';

                mkdir($folder_rec, 0777, true);

                $resize = new AbstractController();
                $path_rec = $folder_rec . date('d') .'_'. date('H') . '-' . date('i') .'-'. date('s') .'-rec-'. $filename;
                copy($path, $path_rec);
                $resize->imageresize(600, $path_rec, $path);
            }else{
                $path_rec = null;
            }

            $files = new Entities\FileManager();
            $files->setName($name);
            $files->setPath($path);
            $files->setPathSec($path_rec);
            $files->setType($type);
            $date = date('d').'-'.date('M').'-'.date('Y');
            $files->setDate($date);

            $em->persist($files);

            $em->flush();
            header("Location: $base_url/admin/files");
            die();
        }
        }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }
    public function filesDeleteAction(){
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
            global $em, $klein, $base_url;
            $id = intval(implode($klein->request()->params(['id'])));

            $files_to_delete = $em->getRepository(Entities\FileManager::class)->findOneBy(array(
                'id' => $id
            ));
            $file_path = $files_to_delete->path;
            $file_path_sec = $files_to_delete->path_sec;
        unlink(getcwd().'/'.$file_path);
            unlink(getcwd().'/'.$file_path_sec);

            $em->remove($files_to_delete);
            $em->flush();

            header("Location: $base_url/admin/files");
            die();
        }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }
}