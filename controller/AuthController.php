<?php

namespace FuryBrains\Controller;

use Database\Entities;
use Google_Client;
use phpbb\auth\provider\oauth\service\google;

class AuthController
{
    /**
     * @var $twig \Twig_Environment
     */
    protected $twig;
//
//    /**
//     * @var \Doctrine\ORM\EntityManager
//     */
//    protected $em;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
//        $this->em = $em;
    }

    public function authGoogleAction(){
        global $em;
        $client_id = '691139384547-no7ibi94fvpu464d1k5uugvdv8e15ov8.apps.googleusercontent.com'; // Client ID
        $client_secret = '9KUl6adO6pgukdkLDG12HLyi'; // Client secret
        $redirect_uri = 'http://furybrains.hol.es/google-auth'; // Redirect URI
        $url_token = 'https://accounts.google.com/o/oauth2/token';
        $url_get_user = 'https://www.googleapis.com/oauth2/v1/userinfo';

        $abstract = new AbstractController();

        if (isset($_GET['code'])) {
            $params = array(
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'redirect_uri' => $redirect_uri,
                'grant_type' => 'authorization_code',
                'code' => $_GET['code']
            );

            $tokenInfo = json_decode($abstract->file_get_contents_curl($url_token, $params), true);
            if (isset($tokenInfo['access_token'])) {
                $params['access_token'] = $tokenInfo['access_token'];

                $userInfo = json_decode(file_get_contents($url_get_user . '?' . urldecode(http_build_query($params))), true);
                if (isset($userInfo['id'])) {
                    $user_google = $em->getRepository(Entities\UsersAPIs::class)->findOneBy(array(
                        'id' => $userInfo['id'] . '-google'
                    ));
                    $users = new Entities\UsersAPIs();
                    $users->setId($userInfo['id'] . '-google');
                    $users->setFirstName($userInfo['given_name']);
                    $users->setLastName($userInfo['family_name']);
                    $users->setPhoto($userInfo['picture']);
                    $users->setEmail($userInfo['email']);
                    $users->setSecurityKey($userInfo['hash']);
                    $users->setLink($userInfo['link']);
                    $users->setLocale($userInfo['locale']);
                    $users->setAuthType('Google+ API');
                    if ($user_google) {
                        $em->merge($users);
                    } else {
                        $em->persist($users);
                    }
                    $em->flush();
                    session_start();
                    $_SESSION['user_id'] = $userInfo['id'] . '-google';
                    $_SESSION['user_photo'] = $userInfo['picture'];
                    $_SESSION['user_firstname'] = $userInfo['given_name'];
                    $_SESSION['user_lastname'] = $userInfo['family_name'];
                    $_SESSION['user_link'] = $userInfo['link'];

                    header('location: news');
                }
            }
        }
    }

    public function authVKAction()
    {
        $client_id = '5843271'; // ID приложения
        $client_secret = 'Da8wNPaedQWemfVoHEH5'; // Защищённый ключ
        $redirect_uri = 'http://furybrains.hol.es/vk-auth'; // Адрес сайта
        $url_oauth = 'https://oauth.vk.com/access_token';
        $url_get_user = 'https://api.vk.com/method/users.get';

        if (isset($_GET['code'])) {
            $params = array(
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'code' => $_GET['code'],
                'redirect_uri' => $redirect_uri
            );

            $get_curl = new AbstractController();
            $token = json_decode($get_curl->file_get_contents_curl($url_oauth, $params), true);

            if (isset($token['access_token'])) {
                $request_params = array(
                    'access_token' => $token['access_token']
                );
                $result = json_decode(($get_curl->file_get_contents_curl($url_get_user.'?', $request_params)));
                if (isset($result['response'][0]['uid'])) {
                    $result = $result['response'][0];
                    $result = true;
                }
            }
        }
    }

    public function authFBAction(){
        global $em;
        $client_id = '243548072722174'; // ID приложения
        $redirect_uri = 'http://furybrains.hol.es/fb-auth'; // Redirect URIs
        $client_secret = '3273784e487e6cb5c04a9d047fcc4317';
        $url_token = 'https://graph.facebook.com/oauth/access_token';
        $url_get_user = 'https://graph.facebook.com/me';

        $abstract = new AbstractController();

        if (isset($_GET['code'])) {
            $params = array(
                'client_id' => $client_id,
                'redirect_uri' => $redirect_uri,
                'client_secret' => $client_secret,
                'code' => $_GET['code']
            );

            $tokenInfo = json_decode($abstract->file_get_contents_curl($url_token, $params), true);
            var_dump($url_token.'?'.http_build_query($params));
            if (isset($tokenInfo['access_token'])) {
                $params = array(
                    'access_token' => $tokenInfo['access_token']
                );
                $userInfo = json_decode($abstract->file_get_contents_curl($url_get_user, $params), true);

                if (isset($userInfo['id'])) {
                    $user_google = $em->getRepository(Entities\UsersAPIs::class)->findOneBy(array(
                        'id' => $userInfo['id'] . '-google'
                    ));
                    $users = new Entities\UsersAPIs();
                    $users->setId($userInfo['id'] . '-google');
                    $users->setFirstName($userInfo['given_name']);
                    $users->setLastName($userInfo['family_name']);
                    $users->setPhoto($userInfo['picture']);
                    $users->setEmail($userInfo['email']);
                    $users->setSecurityKey($userInfo['hash']);
                    $users->setLink($userInfo['link']);
                    $users->setLocale($userInfo['locale']);
                    $users->setAuthType('Google+ API');
                    if ($user_google) {
                        $em->merge($users);
                    } else {
                        $em->persist($users);
                    }
                    $em->flush();
                    session_start();
                    $_SESSION['user_id'] = $userInfo['id'] . '-google';
                    $_SESSION['user_photo'] = $userInfo['picture'];
                    $_SESSION['user_firstname'] = $userInfo['given_name'];
                    $_SESSION['user_lastname'] = $userInfo['family_name'];
                    $_SESSION['user_link'] = $userInfo['link'];

                    header('location: news');
                }
            }
        }
    }

    public function loginAction()
    {
        if (!isset($_SESSION['user_id'])) {
            function fbAuthLink()
            {
                $client_id = '243548072722174'; // ID приложения
                $redirect_uri = 'http://furybrains.hol.es/fb-auth'; // Redirect URIs
                $url = 'https://www.facebook.com/v2.8/dialog/oauth';
                $params = array(
                    'client_id'     => $client_id,
                    'redirect_uri'  => $redirect_uri,
                    'response_type' => 'code',
//                    'scope'         => 'id,first_name'
                );
                $link = $url . '?' . urldecode(http_build_query($params));
                return $link;
            }

            function vkAuthLink()
            {
                $client_id = '5843271'; // ID приложения
                $redirect_uri = 'http://furybrains.hol.es/vk-auth'; // Адрес сайта
                $url = 'https://oauth.vk.com/authorize';
                $params = array(
                    'client_id' => $client_id,
                    'redirect_uri' => $redirect_uri,
                    'response_type' => 'code'
                );
                $link = $url . '?' . urldecode(http_build_query($params));
                return $link;
            }

            function googleAuthLink()
            {
                $client_id = '691139384547-no7ibi94fvpu464d1k5uugvdv8e15ov8.apps.googleusercontent.com'; // Client ID
                $redirect_uri = 'http://furybrains.hol.es/google-auth'; // Redirect URI
                $url = 'https://accounts.google.com/o/oauth2/auth';

                $params = array(
                    'redirect_uri' => $redirect_uri,
                    'response_type' => 'code',
                    'auth_type' => 'request',
                    'client_id' => $client_id,
                    'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
                );
                $link = $url . '?' . urldecode(http_build_query($params));

                return $link;
            }

            echo $this->twig->render('login.twig', array(
                'link_vk_auth' => vkAuthLink(),
                'link_google_auth' => googleAuthLink(),
                'link_fb_auth' => fbAuthLink()
            ));
        }else{
            echo $this->twig->render('errors/404.twig');
        }
    }

    public function exitAction()
    {
        session_start();
        unset($_SESSION['user_id']);
        unset($_SESSION['auth_type']);
        header('Location: news');
        session_destroy();
        die();
    }

    public function registerAction()
    {

    }
}