<?php

namespace FuryBrains\Controller;

use Database\Entities;
use Orbitale\Component\ImageMagick\Command;
use Doctrine\ORM\Tools\Pagination\Paginator;

class AdminController
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

//    /**
//     * @var \Doctrine\ORM\EntityManager
//     */
//    protected $entityManager;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
//        $this->entityManager = $entityManager;
    }

    public function adminIndexAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
            echo $this->twig->render('admin/admin-index.twig');
        }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminNewsAction(){
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em;
            $category = $_GET['category'];

            $page = $_GET['page']-1;
            if($page < 1){
                $page = 0;
            }
            $results = 10;

            $news_all = $em->getRepository(Entities\News::class)->findAll();

            if($category>0){
                $news = $em->getRepository(Entities\News::class)->createQueryBuilder('s')
                    ->select("s")
                    ->setFirstResult($page*10)
                    ->setMaxResults($results)
                    ->where("s.category = $category")
                    ->orderBy('s.id', 'DESC')
                    ->getQuery()
                    ->getResult();
                $pages = intval(count($news)/$results);
            }else{
                $news = $em->getRepository(Entities\News::class)->createQueryBuilder('s')
                    ->select("s")
                    ->setFirstResult($page*10)
                    ->setMaxResults($results)
                    ->orderBy('s.id', 'DESC')
                    ->getQuery()
                    ->getResult();
                $pages = intval(count($news_all)/$results);
            }
            $categories = $em->getRepository(Entities\CategoriesNews::class)->findAll();
            echo $this->twig->render('admin/news/admin-news.twig', array(
                    'news' => $news,
                    'categories' => $categories,
                    'pages_number' => $pages+1,
                    'current_page' => $page+1,
                    'category_selected' => $category
                ));
            }
    else {
        echo $this->twig->render('errors/403.twig');
    }
}

    public function adminNewsCategoriesAction(){
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em, $base_url;
                $categories = $em->getRepository(Entities\CategoriesNews::class)->findAll();

                if (isset($_POST['title']) && isset($_POST['query'])) {
                    $categ = new Entities\CategoriesNews();
                    $category_title = htmlspecialchars($_POST['title']);
                    $category_query = htmlspecialchars($_POST['query']);
                    $categ->setTitle($category_title);
                    $categ->setQuery($category_query);
                    $em->persist($categ);
                    $em->flush();
                    header("Location: $base_url/admin/news/categories");
                    die();
                }
                echo $this->twig->render('admin/news/admin-news-categories.twig', array(
                    'categories' => $categories
                ));
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }
    public function adminNewsAddAction(){
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em, $base_url;
                $categories = $em->getRepository(Entities\CategoriesNews::class)->findAll();
                echo $this->twig->render('admin/news/admin-news-add.twig', array(
                    'categories' => $categories
                ));
                if (isset($_POST['title']) and isset($_POST['description'])) {
                    $category = $em->getRepository(Entities\CategoriesNews::class)->findOneBy(['id' => $_POST['category']]);

                    $title = htmlspecialchars($_POST['title']);
                    $description = htmlspecialchars($_POST['description']);
                    $rate = htmlspecialchars($_POST['rate']);
                    $front_img = htmlspecialchars($_POST['front_image']);
                    $text = htmlspecialchars($_POST['text']);
                    $tags = htmlspecialchars($_POST['tags']);
                    $date = date('d').'-'.date('M').'-'.date('Y');

                    $news = new Entities\News();
                    $news->setTitle($title);
                    $news->setCategory($em->getRepository(Entities\CategoriesNews::class)->findOneBy(['id' => $category->id]));
                    $news->setDescription($description);
                    $news->setRate($rate);
                    $news->setText($text);
                    $news->setTags($tags);
                    $news->setDate($date);
                    $news->setFrontImg($front_img);

                    $em->persist($news);

                    $em->flush();
                    header("Location: $base_url/admin/news");
                    die();
                }
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }
    public function adminNewsEditAction(){
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em, $base_url, $klein;
                $id = intval(implode($klein->request()->params(['id'])));
                $news_edit = $em->getRepository(Entities\News::class)->findOneBy(['id' => $id]);

            $categories = $em->getRepository(Entities\CategoriesNews::class)->findAll();
                if (isset($_POST['category'])) {
                    $category = intval(htmlspecialchars($_POST['category']));
                }
                if (isset($_POST['title'])) {
                    $front_img = htmlspecialchars($_POST['front_image']);
                    $title = htmlspecialchars($_POST['title']);
                    $description = htmlspecialchars($_POST['description']);
                    $text = htmlspecialchars($_POST['text']);
                    $tags = htmlspecialchars($_POST['tags']);
                    $rate = htmlspecialchars($_POST['rate']);
                    $date = date('d').'-'.date('M').'-'.date('Y');

                    $news = new Entities\News();
                    $news->setId($id);
                    $news->setTitle($title);
                    $news->setCategory($em->getRepository(Entities\CategoriesNews::class)->findOneBy(['id' => $category]));
                    $news->setDescription($description);
                    $news->setText($text);
                    $news->setTags($tags);
                    $news->setFrontImg($front_img);
                    $news->setRate($rate);
                    $news->setDate($date);

                    $em->merge($news);

                    $em->flush();
                    header("Location: $base_url/admin/news");
                    die();
                }
                echo $this->twig->render('admin/news/admin-news-edit.twig', array(
                    'news_edit' => $news_edit,
                    'categories' => $categories

                ));
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }

    }
    public function adminNewsDeleteAction(){
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em, $klein, $base_url;
                $id = intval(implode($klein->request()->params(['id'])));
                if ($em->getRepository(Entities\Rates::class)->findOneBy(
                    array('post' => $id, 'user' => $_SESSION['user_id'])
                )
                ) {
                    $rates = $em->getRepository(Entities\Rates::class)->findOneBy(
                        array('post' => $id, 'user' => $_SESSION['user_id'])
                    );
                    $em->remove($rates);
                    $em->flush();
                }

                $likes = $em->getRepository(\Database\Entities\Rates::class)->createQueryBuilder('a')
                    ->select('a')
                    ->where("a.post = $id")
                    ->getQuery();
                $comments = $em->getRepository(\Database\Entities\CommentsNews::class)->createQueryBuilder('a')
                    ->select('a')
                    ->where("a.post = $id")
                    ->getQuery();
                $likes = $likes->execute();
                foreach ($likes as $result) {
                    $em->remove($result);
                }
                $comments = $comments->execute();
                foreach ($comments as $result) {
                    $em->remove($result);
                }

                $news = $em->getPartialReference(Entities\News::class, array('id' => $id));
                $em->remove($news);
                $em->flush();

                header("Location: $base_url/admin/news");
                die();
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminNewsCommentsAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em;
                $comments = $em->getRepository(Entities\CommentsNews::class)->createQueryBuilder('a')
                    ->select('a')
                    ->orderBy('a.id', 'DESC')
                    ->getQuery()
                    ->getResult();
                echo $this->twig->render('admin/news/admin-news-comments.twig', array(
                    'comments' => $comments
                ));
        } else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminNewsCommentsEditAction(){
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em, $base_url, $klein;
                $id = intval(implode($klein->request()->params(['id'])));
                $comments_edit = $em->getRepository(Entities\CommentsNews::class)->findOneBy(['id' => $id]);

                if (isset($_POST['text'])) {

                    $text = htmlspecialchars($_POST['text']);
                    $post = intval(htmlspecialchars($_POST['post']));
                    $user = intval(htmlspecialchars($_POST['user']));

                    $comments = new Entities\CommentsNews();
                    $comments->setId($id);
                    $comments->setText($text);
                    $comments->setPost($em->getRepository(Entities\News::class)->findOneBy(['id' => $post]));
                    $comments->setUser($em->getRepository(Entities\Users::class)->findOneBy(['id' => $user]));

                    $em->merge($comments);
                    $em->flush();
                    header("Location: $base_url/admin/news/comments");
                    die();
                }
                echo $this->twig->render('admin/news/admin-news-comments-edit.twig', array(
                    'comments_edit' => $comments_edit,
                ));
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }

    }

    public function adminNewsCommentsDeleteAction(){
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em, $klein, $base_url;
                $id = intval(implode($klein->request()->params(['id'])));
                $comments = $em->getPartialReference(Entities\CommentsNews::class, array('id' => $id));
                $em->remove($comments);
                $em->flush();

                header("Location: $base_url/admin/news/comments");
                die();
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminBannersAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                echo $this->twig->render('admin/banners.twig', array());
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }
    public function adminBannersAddAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){

                global $em, $base_url;

                echo $this->twig->render('admin/banners-add.twig', array());

                if (isset($_POST['title']) and isset($_POST['text'])) {

                    $folder = 'upload/banners';

                    $path = $folder . time() . '-' . $_FILES["image"]["name"];

                    $title = htmlspecialchars($_POST['title']);
                    $text = htmlspecialchars($_POST['text']);
                    $link = htmlspecialchars($_POST['link']);
                    $date_start = htmlspecialchars($_POST['date_start']);
                    $date_end = htmlspecialchars($_POST['date_end']);

                    move_uploaded_file($_FILES["image"]["tmp_name"], $path);

                    $image = htmlspecialchars($path);

                    $banners = new Entities\Banners();
                    $date_start = new \DateTime($date_start);
                    $date_end = new \DateTime($date_end);
                    $banners->setTitle($title);
                    $banners->setText($text);
                    $banners->setImage($image);
                    $banners->setLink($link);
                    $banners->setDateStart($date_start);
                    $banners->setDateEnd($date_end);

                    $em->persist($banners);

                    $em->flush();
                    header("Location: $base_url/admin/banners");
                    die();
                }
        }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }
    public function adminBannersEditAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){

                global $em, $base_url, $klein;
                $id = intval(implode($klein->request()->params(['id'])));
                $banners_edit = $em->getRepository(Entities\Banners::class)->findOneBy(['id' => $id]);

                $last_image = $banners_edit->image;

                echo $this->twig->render('admin/banners-edit.twig', array(
                    'banners_edit' => $banners_edit
                ));

                if (isset($_POST['title']) and isset($_POST['text'])) {

                    $folder = 'upload/banners';

                    $path = $folder . time() . '-' . $_FILES["image"]["name"];

                    $title = htmlspecialchars($_POST['title']);
                    $text = htmlspecialchars($_POST['text']);
                    $link = htmlspecialchars($_POST['link']);
                    $date_start = htmlspecialchars($_POST['date_start']);
                    $date_end = htmlspecialchars($_POST['date_end']);

                    move_uploaded_file($_FILES["image"]["tmp_name"], $path);

                    $image = htmlspecialchars($path);

                    $banners = new Entities\Banners();
                    $date_start = new \DateTime($date_start);
                    $date_end = new \DateTime($date_end);
                    $banners->setId($id);
                    $banners->setTitle($title);
                    $banners->setText($text);
                    $banners->setImage($image);
                    $banners->setLink($link);
                    $banners->setDateStart($date_start);
                    $banners->setDateEnd($date_end);
                    if (!empty($_FILES["image"]["tmp_name"])) {
                        $banners->setImage($image);
                    } else {
                        $banners->setImage($last_image);
                    }
                    $em->merge($banners);
                    $em->flush();
                    header("Location: $base_url/admin/banners");
                    die();
                }
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminBannersDeleteAction(){
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em, $klein, $base_url;
                $id = intval(implode($klein->request()->params(['id'])));
                $comments = $em->getPartialReference(Entities\Banners::class, array('id' => $id));
                $em->remove($comments);
                $em->flush();

                header("Location: $base_url/admin/banners");
                die();
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminFooterAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em;
                $footer = $em->getRepository(Entities\Footer::class)->createQueryBuilder('a')
                    ->select('a')
                    ->orderBy('a.id', 'DESC')
                    ->getQuery()
                    ->getResult();
                echo $this->twig->render('admin/footer.twig', array(
                    'footer' => $footer
                ));
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminFooterAddAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){

                global $em, $base_url;

                echo $this->twig->render('admin/footer-add.twig', array());

                if (isset($_POST['text']) and isset($_POST['link'])) {

                    $text = htmlspecialchars($_POST['text']);
                    $link = htmlspecialchars($_POST['link']);

                    $footer = new Entities\Footer();

                    $footer->setText($text);
                    $footer->setLink($link);

                    $em->persist($footer);

                    $em->flush();
                    header("Location: $base_url/admin/footer");
                    die();
                }
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminFooterEditAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){

                global $em, $base_url, $klein;
                $id = intval(implode($klein->request()->params(['id'])));
                $footer_edit = $em->getRepository(Entities\Footer::class)->findOneBy(['id' => $id]);

                echo $this->twig->render('admin/footer-edit.twig', array(
                    'footer_edit' => $footer_edit
                ));

                if (isset($_POST['text']) and isset($_POST['link'])) {

                    $text = htmlspecialchars($_POST['text']);
                    $link = htmlspecialchars($_POST['link']);

                    $footer = new Entities\Footer();

                    $footer->setId($id);
                    $footer->setText($text);
                    $footer->setLink($link);

                    $em->merge($footer);
                    $em->flush();
                    header("Location: $base_url/admin/footer");
                    die();
                }
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminFooterDeleteAction(){
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em, $klein, $base_url;
                $id = intval(implode($klein->request()->params(['id'])));
                $footer = $em->getPartialReference(Entities\Footer::class, array('id' => $id));
                $em->remove($footer);
                $em->flush();

                header("Location: $base_url/admin/footer");
                die();
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminRecommendedAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em;
                $recommended = $em->getRepository(Entities\Recommended::class)->createQueryBuilder('a')
                    ->select('a')
                    ->orderBy('a.id', 'DESC')
                    ->getQuery()
                    ->getResult();
                echo $this->twig->render('admin/recommended.twig', array(
                    'recommended' => $recommended
                ));
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminRecommendedAddAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){

                global $em, $base_url;

                echo $this->twig->render('admin/recommended-add.twig', array());

                if (isset($_POST['title']) and isset($_POST['link'])) {

                    $title = htmlspecialchars($_POST['title']);
                    $image = htmlspecialchars($_POST['image']);
                    $link = htmlspecialchars($_POST['link']);
                    $position = htmlspecialchars($_POST['position']);

                    $recommended = new Entities\Recommended();

                    $recommended->setTitle($title);
                    $recommended->setImage($image);
                    $recommended->setLink($link);
                    $recommended->setPosition($position);

                    $em->persist($recommended);

                    $em->flush();
                    header("Location: $base_url/admin/recommended");
                    die();
                }
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminRecommendedEditAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){

                global $em, $base_url, $klein;
                $id = intval(implode($klein->request()->params(['id'])));
                $recommended_edit = $em->getRepository(Entities\Recommended::class)->findOneBy(['id' => $id]);

                echo $this->twig->render('admin/recommended-edit.twig', array(
                    'recommended_edit' => $recommended_edit
                ));

                if (isset($_POST['title']) and isset($_POST['link'])) {

                    $title = htmlspecialchars($_POST['title']);
                    $image = htmlspecialchars($_POST['image']);
                    $link = htmlspecialchars($_POST['link']);
                    $position = htmlspecialchars($_POST['position']);

                    $recommended = new Entities\Recommended();

                    $recommended->setId($id);
                    $recommended->setTitle($title);
                    $recommended->setImage($image);
                    $recommended->setLink($link);
                    $recommended->setPosition($position);

                    $em->merge($recommended);
                    $em->flush();
                    header("Location: $base_url/admin/recommended");
                    die();
                }
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminRecommendedDeleteAction(){

        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em, $klein, $base_url;
                $id = intval(implode($klein->request()->params(['id'])));
                $recommended = $em->getPartialReference(Entities\Recommended::class, array('id' => $id));
                $em->remove($recommended);
                $em->flush();

                header("Location: $base_url/admin/recommended");
                die();
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminUsersAction()
    {

        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em;
                $users = $em->getRepository(Entities\UsersAPIs::class)->createQueryBuilder('a')
                    ->select('a')
                    ->orderBy('a.id', 'DESC')
                    ->getQuery()
                    ->getResult();
                echo $this->twig->render('admin/users.twig', array(
                    'users' => $users
                ));
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminUsersEditAction()
    {
        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){

                global $em, $base_url, $klein;
                $id = intval(implode($klein->request()->params(['id'])));
                $users_edit = $em->getRepository(Entities\UsersAPIs::class)->findOneBy(['id' => $id]);

                $last_photo = $users_edit->photo;

                echo $this->twig->render('admin/users-edit.twig', array(
                    'users_edit' => $users_edit
                ));

                if (isset($_POST['password']) and isset($_POST['password_confirm'])) {

                    $folder = 'upload/users/avatars';

                    $path = $folder . time() . '-' . $_FILES["image"]["name"];

                    $email = htmlspecialchars($_POST['email']);
                    $login = htmlspecialchars($_POST['login']);

                    $password = md5(htmlspecialchars($_POST['password']));
                    $password_r = md5(htmlspecialchars($_POST['password_confirm']));
                    $first_name = htmlspecialchars($_POST['first_name']);
                    $last_name = htmlspecialchars($_POST['last_name']);
                    $site = htmlspecialchars($_POST['site']);

                    move_uploaded_file($_FILES["image"]["tmp_name"], $path);

                    $photo = htmlspecialchars($path);

                    $users = new Entities\Users();

                    $users->setId($id);
                    $users->setEmail($email);
                    $users->setLogin($login);
                    $users->setPassword($password);
                    $users->setPasswordR($password_r);
                    $users->setPhoto($photo);
                    $users->setFirstName($first_name);
                    $users->setLastName($last_name);
                    $users->setSite($site);

                    if (!empty($_FILES["image"]["tmp_name"])) {
                        $users->setPhoto($photo);
                    } else {
                        $users->setPhoto($last_photo);
                    }
                    $em->merge($users);
                    $em->flush();
                    header("Location: $base_url/admin/users");
                    die();
                }
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminUsersDeleteAction(){

        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
                global $em, $klein, $base_url;
                $id = intval(implode($klein->request()->params(['id'])));

            $likes = $em->getRepository(\Database\Entities\Rates::class)->createQueryBuilder('a')
                ->select('a')
                ->where("a.user = $id")
                ->getQuery();
            $comments = $em->getRepository(\Database\Entities\CommentsNews::class)->createQueryBuilder('a')
                ->select('a')
                ->where("a.user = $id")
                ->getQuery();
            $likes = $likes->execute();
            foreach ($likes as $result) {
                $em->remove($result);
            }
            $comments = $comments->execute();
            foreach ($comments as $result) {
                $em->remove($result);
            }

            $user = $em->getPartialReference(Entities\UsersAPIs::class, array('id' => $id));

            $em->remove($user);
            $em->flush();

                header("Location: $base_url/admin/users");
                die();
            }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminFeedbackAction()
    {

        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
            global $em;
            $feedback = $em->getRepository(Entities\Feedback::class)->createQueryBuilder('a')
                ->select('a')
                ->orderBy('a.id', 'DESC')
                ->getQuery()
                ->getResult();
            echo $this->twig->render('admin/feedback.twig', array(
                'feedback' => $feedback
            ));
        }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }

    public function adminFeedbackDeleteAction(){

        if($_SESSION['user_id'] == '113224640764410373594-google' or $_SESSION['user_id'] == '105291039154021851436-google'){
            global $em, $klein, $base_url;
            $id = intval(implode($klein->request()->params(['id'])));
            $feedback = $em->getPartialReference(Entities\Feedback::class, array('id' => $id));
            $em->remove($feedback);
            $em->flush();

            header("Location: $base_url/admin/feedback");
            die();
        }
        else {
            echo $this->twig->render('errors/403.twig');
        }
    }
}
?>