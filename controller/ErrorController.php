<?php

namespace FuryBrains\Controller;

use Database\Entities;

class ErrorController
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

//    /**
//     * @var \Doctrine\ORM\EntityManager
//     */
//    protected $entityManager;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
//        $this->entityManager = $entityManager;
    }

    public function error404Action(){
        global $em, $twig;
        echo $this->twig->render('errors/404.twig', array(
//            'news_android' => $news_android
        ));
    }

    public function error403Action(){
        global $em, $twig;
        echo $this->twig->render('errors/403.twig', array(
//            'news_android' => $news_android
        ));
    }


}