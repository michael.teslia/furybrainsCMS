<?php

$index_controller = new \FuryBrains\Controller\IndexController($twig);
$auth_controller = new \FuryBrains\Controller\AuthController($twig);
$admin_controller = new \FuryBrains\Controller\AdminController($twig);
$error_controller = new \FuryBrains\Controller\ErrorController($twig);
$ajax_controller = new \FuryBrains\Controller\AjaxController($twig);
$files_controller = new \FuryBrains\Controller\FilesController($twig);

$base  = dirname($_SERVER['PHP_SELF']);

global $em;

if(ltrim($base, '/')){
    $_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], strlen($base));
}

$klein = new \Klein\Klein();

$klein->onHttpError(function ($code) use ($error_controller) {
    switch ($code) {
        case 404:
            $error_controller->error404Action();
            break;
        case 403:
            $error_controller->error403Action();
            break;
    }
});
const TECHNOLOGIES = 1;
const WEB = 2;
const MOBILE = 3;

$klein->respond('POST', '/rate/[i:id]', function () use ($twig, $ajax_controller) {
    $ajax_controller->rateAction();
});
$klein->respond('POST', '/comment-send/[i:id]', function () use ($twig, $ajax_controller) {
    $ajax_controller->commentSendAction();
});
$klein->respond('POST', '/feedback-send', function () use ($twig, $ajax_controller) {
    $ajax_controller->feedbackSendAction();
});
$klein->respond('GET', '/google-auth', function () use ($twig, $auth_controller) {
    $auth_controller->authGoogleAction();
});
$klein->respond('GET', '/vk-auth', function () use ($twig, $auth_controller) {
    $auth_controller->authVKAction();
});
$klein->respond('GET', '/fb-auth', function () use ($twig, $auth_controller) {
    $auth_controller->authFBAction();
});

for($k=1;$k<=3;$k++) {
    $klein->respond('POST', "/category-load-up-".$k, function () use ($twig, $ajax_controller, $k) {
        $ajax_controller->newsCategoryScrollAction($k);
    });
}
$klein->respond('POST', "/index-load-up", function () use ($twig, $ajax_controller) {
    $ajax_controller->newsIndexScrollAction();
});






$klein->respond('GET', '/search', function () use ($twig, $index_controller) {
    $index_controller->searchAction();
});

$klein->respond('GET', '/exit', function () use ($twig, $auth_controller) {
    $auth_controller->exitAction();
});

// routes for ADMIN PANEL
$klein->respond('GET', '/admin', function () use ($twig, $admin_controller) {
    $admin_controller->adminIndexAction();
});

$klein->respond('GET', '/admin/news', function () use ($twig, $admin_controller) {
    $admin_controller->adminNewsAction();
});
$klein->respond('/admin/news/add', function () use ($twig, $admin_controller) {
    $admin_controller->adminNewsAddAction();
});
$klein->respond('/admin/news/edit/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminNewsEditAction();
});
$klein->respond('/admin/news/delete/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminNewsDeleteAction();
});

$klein->respond('/admin/news/categories', function () use ($twig, $admin_controller) {
    $admin_controller->adminNewsCategoriesAction();
});

$klein->respond('/admin/news/comments', function () use ($twig, $admin_controller) {
    $admin_controller->adminNewsCommentsAction();
});
$klein->respond('/admin/news/comments/edit/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminNewsCommentsEditAction();
});
$klein->respond('/admin/news/comments/delete/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminNewsCommentsDeleteAction();
});

$klein->respond('/admin/banners', function () use ($twig, $admin_controller) {
    $admin_controller->adminBannersAction();
});
$klein->respond('/admin/banners/add', function () use ($twig, $admin_controller) {
    $admin_controller->adminBannersAddAction();
});
$klein->respond('/admin/banners/edit/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminBannersEditAction();
});
$klein->respond('/admin/banners/delete/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminBannersDeleteAction();
});

$klein->respond('/admin/recommended', function () use ($twig, $admin_controller) {
    $admin_controller->adminRecommendedAction();
});
$klein->respond('/admin/recommended/add', function () use ($twig, $admin_controller) {
    $admin_controller->adminRecommendedAddAction();
});
$klein->respond('/admin/recommended/edit/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminRecommendedEditAction();
});
$klein->respond('/admin/recommended/delete/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminRecommendedDeleteAction();
});

$klein->respond('/admin/users', function () use ($twig, $admin_controller) {
    $admin_controller->adminUsersAction();
});
$klein->respond('/admin/users/edit/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminUsersEditAction();
});
$klein->respond('/admin/users/delete/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminUsersDeleteAction();
});

$klein->respond('/admin/feedback', function () use ($twig, $admin_controller) {
    $admin_controller->adminFeedbackAction();
});
$klein->respond('/admin/feedback/delete/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminFeedbackDeleteAction();
});
$klein->respond('/admin/footer', function () use ($twig, $admin_controller) {
    $admin_controller->adminFooterAction();
});
$klein->respond('/admin/footer/add', function () use ($twig, $admin_controller) {
    $admin_controller->adminFooterAddAction();
});
$klein->respond('/admin/footer/edit/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminFooterEditAction();
});
$klein->respond('/admin/footer/delete/[i:id]', function () use ($twig, $admin_controller) {
    $admin_controller->adminFooterDeleteAction();
});
//FILE MANAGER
$klein->respond('/admin/files', function () use ($twig, $files_controller) {
    $files_controller->filesIndexAction();
});
$klein->respond('/admin/files/add', function () use ($twig, $files_controller) {
    $files_controller->filesAddAction();
});
$klein->respond('/admin/files/delete/[i:id]', function () use ($twig, $files_controller) {
    $files_controller->filesDeleteAction();
});

$base = $klein->respond('GET', '/', function () use ($twig, $index_controller) {
//    $index_controller->newsAction();
    header('Location: news');
});

$klein->respond('GET', '/news', function () use ($twig, $index_controller) {
    $index_controller->newsAction();
});
$klein->respond('GET', '/news/technologies', function () use ($twig, $index_controller) {
    $index_controller->newsCategoryAction($k = TECHNOLOGIES);
});
$klein->respond('GET', '/news/technologies/[i:id]', function () use ($twig, $index_controller) {
    $index_controller->newsPostFullAction($k = TECHNOLOGIES);
});

$klein->respond('GET', '/news/web', function () use ($twig, $index_controller) {
    $index_controller->newsCategoryAction($k = WEB);
});
$klein->respond('GET', '/news/web/[i:id]', function () use ($twig, $index_controller) {
    $index_controller->newsPostFullAction($k = WEB);
});

$klein->respond('GET', '/news/mobile', function () use ($twig, $index_controller, $ajax_controller) {
    $index_controller->newsCategoryAction($k = MOBILE);
});
$klein->respond('GET', '/news/mobile/[i:id]', function () use ($twig, $index_controller) {
    $index_controller->newsPostFullAction($k = MOBILE);
});

$klein->respond('GET', '/login', function () use ($twig, $auth_controller) {
    $auth_controller->loginAction();
});

$klein->respond('GET', '/profile', function () use ($twig, $index_controller) {
    $index_controller->profileIndexAction();
});

$klein->respond('GET', '/develop', function () use ($twig, $index_controller) {
    $index_controller->developAction();
});

$klein->respond('GET', '/about-us', function () use ($twig, $index_controller) {
    $index_controller->aboutUsAction();
});


$klein->respond('GET', '/feedback', function () use ($twig, $index_controller) {
    $index_controller->feedbackAction();
});

$twig->addGlobal('url', $_SERVER['REQUEST_URI']);


$klein->dispatch();

