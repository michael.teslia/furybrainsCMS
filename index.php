<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/db.php';
require_once __DIR__ . '/controller/IndexController.php';
require_once __DIR__ . '/controller/AuthController.php';
require_once __DIR__ . '/controller/admin/AdminController.php';
require_once __DIR__ . '/controller/ErrorController.php';
require_once __DIR__ . '/controller/AJAX/AjaxController.php';
require_once __DIR__ . '/controller/AbstractController.php';
require_once __DIR__ . '/controller/FilesController.php';

session_start();

$twig =  new \Twig_Environment(
    new Twig_Loader_Filesystem(array(
    __DIR__. '/templates'
    )),
    array(
        'debug' => true,
//        'cache' =>  __DIR__. '/cache'
    )
);
global $em;

$twig->addExtension(new Twig_Extension_Debug());

$array_uri = explode('/', $_SERVER['PHP_SELF']);
unset($array_uri[count($array_uri)-1]);
$base_url = implode('/', $array_uri);
$twig->addGlobal('project_root', $base_url);

if(isset($_SESSION['user_id'])) {
    $twig->addGlobal('user_id', $_SESSION['user_id']);
}
if(isset($_SESSION['user_firstname'])) {
    $twig->addGlobal('user_firstname', $_SESSION['user_firstname']);
}
if(isset($_SESSION['user_lastname'])) {
    $twig->addGlobal('user_lastname', $_SESSION['user_lastname']);
}
if(isset($_SESSION['user_photo'])) {
    $twig->addGlobal('user_photo', $_SESSION['user_photo']);
}
if(isset($_SESSION['user_link'])) {
    $twig->addGlobal('user_link', $_SESSION['user_link']);
}
$categories_news = $em->getRepository(\Database\Entities\CategoriesNews::class)->createQueryBuilder('a')
    ->select('a')
    ->getQuery()
    ->getResult();
$twig->addGlobal('categories_news', $categories_news);

$comments = $em->getRepository(\Database\Entities\CommentsNews::class)->createQueryBuilder('a')
    ->select('a')
    ->setMaxResults(10)
    ->orderBy('a.id', 'DESC')
    ->getQuery()
    ->getResult();
$comments_number = count($comments);
$twig->addGlobal('comments_all', $comments);

$news = $em->getRepository(\Database\Entities\News::class)->createQueryBuilder('a')
    ->select('a')
    ->setMaxResults(10)
    ->orderBy('a.rate', 'DESC')
    ->getQuery()
    ->getResult();
$twig->addGlobal('news_rate_left', $news);

$banners = $em->getRepository(\Database\Entities\Banners::class)->createQueryBuilder('a')
    ->select('a')
    ->getQuery()
    ->getResult();
$twig->addGlobal('banners', $banners);

$footer = $em->getRepository(\Database\Entities\Footer::class)->createQueryBuilder('a')
    ->select('a')
    ->orderBy('a.id', 'DESC')
    ->getQuery()
    ->getResult();
$twig->addGlobal('footer', $footer);

$recommended_pos_1 = $em->getRepository(\Database\Entities\Recommended::class)->createQueryBuilder('a')
    ->select('a')
    ->where('a.position = 1')
    ->setMaxResults(10)
    ->orderBy('a.id', 'DESC')
    ->getQuery()
    ->getResult();
$twig->addGlobal('recommended_pos_1', $recommended_pos_1);

//if(isset($_SESSION['uid'])) {
//    $user_id = $_SESSION['uid'];
//    $user_comments = $em->getRepository(\Database\Entities\CommentsNews::class)->createQueryBuilder('a')
//        ->select('a')
//        ->where("a.user = $user_id")
////        ->setMaxResults(5)
//        ->orderBy('a.id', 'DESC')
//        ->getQuery()
//        ->getResult();
//    $twig->addGlobal('user_comments', $user_comments);
//    $twig->addGlobal('user_comments_num', count($user_comments));
//    if(isset($news)) {
//        $user_likes = $em->getRepository(\Database\Entities\Rates::class)->createQueryBuilder('a')
//            ->select('a')
//            ->where("a.user = $user_id")
////            ->setMaxResults(5)
//            ->orderBy('a.id', 'DESC')
//            ->getQuery()
//            ->getResult();
//        $twig->addGlobal('user_likes', $user_likes);
//        $twig->addGlobal('user_likes_num', count($user_likes));
//    }
//}

include 'routes.php';

$klein->request()->id();


