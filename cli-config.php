<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap
require_once 'db.php';

// replace with mechanism to retrieve EntityManager in your app

global $em;

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($em);
