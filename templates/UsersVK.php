<?php

namespace Database\Entities;

use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="users_vk")
 *
 **/

class UsersVK
{
    /**
     * @Id
     * @Column(type="string", name="uid")
     * @OneToMany(targetEntity="CommentsNews", mappedBy="user")
     * @OneToMany(targetEntity="Rates", mappedBy="user")
     */
    public $uid;
    /**
     * @Column(type="string", name="firstname")
     * @var string
     */
    public $first_name;
    /**
     * @Column(type="string", name="lastname")
     * @var string
     */
    public $last_name;
    /**
     * @Column(type="string", name="photo", nullable=true)
     * @var string
     */
    public $photo;
    /**
     * @Column(type="string", name="hash")
     * @var string
     */
    protected $hash;


    public function __construct()
    {
        $this->uid = new ArrayCollection();
    }

    public function getUid()
    {
        return $this->uid;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function setUid($uid)
    {
        $this->uid = $uid;
    }
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

}