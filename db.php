<?php

use Doctrine\ORM\EntityRepository;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once 'database/entities/News.php';
require_once 'database/entities/CategoriesNews.php';
require_once 'database/entities/CommentsNews.php';
require_once 'database/entities/Development.php';
require_once 'database/entities/CategoriesDevelopment.php';
require_once 'database/entities/CommentsDevelopment.php';
require_once 'database/entities/Feedback.php';
require_once 'database/entities/AboutUs.php';
require_once 'database/entities/Users.php';
require_once 'database/entities/UsersAPIs.php';
require_once 'database/entities/Contacts.php';
require_once 'database/entities/Rates.php';
require_once 'database/entities/FileManager.php';
require_once 'database/entities/Banners.php';
require_once 'database/entities/Recommended.php';
require_once 'database/entities/Footer.php';

$paths = array("database/entities");

// автоматическая генерация Proxy, которые использует Doctrine ORM
$isDevMode = true;

// the connection configuration
$dbParams = array(
    'host' => 'mysql.hostinger.com.ua',
    'driver' => 'pdo_mysql',
    'user' => 'u332852332_mikz',
    'password' => 'RkZDdx@+1Zh&w&/ee2',
    'dbname' => 'u332852332_fury',
    'charset' => 'utf8'
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$em = EntityManager::create($dbParams, $config);

