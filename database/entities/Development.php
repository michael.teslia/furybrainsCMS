<?php

namespace Database\Entities;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="development")
 *
 **/

class Development
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    /**
     * @Column(type="string", name="title")
     * @OneToMany(targetEntity="CommentsDevelopment", mappedBy="post")
     * @var string
     */
    protected $title;
    /**
     * @ManyToOne(targetEntity="CategoriesDevelopment", inversedBy="title")
     * @var string
     */
    protected $category;
    /**
     * @Column(type="string", name="description")
     * @var string
     */
    protected $description;
    /**
     * @Column(type="string", name="text")
     * @var string
     */
    protected $text;
    /**
     * @Column(type="date", name="date")
     * @var string
     */
    protected $date;
    /**
     * @Column(type="string", name="img1")
     * @var string
     */
    public $img1;
    /**
     * @Column(type="string", name="img2")
     * @var string
     */
    public $img2;
    /**
     * @Column(type="string", name="img3")
     * @var string
     */
    public $img3;
    /**
     * @Column(type="string", name="img4")
     * @var string
     */
    public $img4;
    /**
     * @Column(type="string", name="img5")
     * @var string
     */
    public $img5;

    public function __construct()
    {
        $this->id = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id->toArray();
    }



    public function setId($id)
    {
        $this->id = $id;
    }


}