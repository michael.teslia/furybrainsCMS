<?php

namespace Database\Entities;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="comments_development")
 *
 **/

class CommentsDevelopment
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    public $id;
    /**
     * @ManyToOne(targetEntity="Development", inversedBy="login")
     * @var string
     */
    public $user;
    /**
     * @ManyToOne(targetEntity="Development", inversedBy="title")
     * @var string
     */
    protected $post;
    /**
     * @Column(type="string", name="text")
     * @var string
     */
    protected $text;


}