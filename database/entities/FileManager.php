<?php
/**
 * Created by PhpStorm.
 * User: mikzt
 * Date: 12.02.2017
 * Time: 10:47
 */

namespace Database\Entities;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="files")
 *
 **/

class FileManager
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @Column(type="string", name="name", length=100)
     * @var string
     */
    public $name;

    /**
     * @Column(type="string", name="path", length=100)
     * @var string
     */
    public $path;

    /**
     * @Column(type="string", name="path_sec", length=100, nullable=true)
     * @var string
     */
    public $path_sec;

    /**
     * @Column(type="string", name="type", length=100)
     * @var string
     */
    public $type;

    /**
     * @Column(type="string", name="date", length=100)
     * @var string
     */
    public $date;

    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPath()
    {
        return $this->path;
    }
    public function getPathSec()
    {
        return $this->path_sec;
    }
    public function getType()
    {
        return $this->type;
    }
    public function getDate()
    {
        return $this->date;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setPath($path)
    {
        $this->path = $path;
    }
    public function setPathSec($path_sec)
    {
        $this->path_sec = $path_sec;
    }
    public function setType($type)
    {
        $this->type = $type;
    }
    public function setDate($date)
    {
        $this->date = $date;
    }
}