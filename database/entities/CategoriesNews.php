<?php

namespace Database\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="categories_news")
 *
 **/

class CategoriesNews
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    public $id;
    /**
     * @OneToMany(targetEntity="News", mappedBy="category")
     * @Column(type="string", name="title")
     */
    public $title;
    /**
     * @Column(type="string", name="query")
     */
    public $query;
    /**
     * @Column(type="string", name="image")
     */
    public $image;

    public function __construct()
    {
        $this->title = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }
    public function getTitle()
    {
        return $this->title;
    }
    public function getQuery()
    {
        return $this->query;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }
    public function setQuery($query)
    {
        $this->query = $query;
    }
}