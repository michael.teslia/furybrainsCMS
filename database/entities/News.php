<?php

namespace Database\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="news")
 *
 **/

class News
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     * @OneToMany(targetEntity="CommentsNews", mappedBy="post")
     * @OneToMany(targetEntity="Rates", mappedBy="post")
     */
    public $id;
    /**
     * @Column(type="string", name="title", length=255)
     * @var string
     */
    public $title;
    /**
     * @ManyToOne(targetEntity="CategoriesNews", inversedBy="title")
     * @JoinColumn(name="category_id", referencedColumnName="id")
     */
    public $category;
    /**
     * @Column(type="string", name="description", length=1000)
     * @var string
     */
    public $description;
    /**
     * @Column(type="string", name="text", length=20000)
     * @var string
     */
    public $text;

    /**
     * @Column(type="string", name="date", nullable=true)
     * @var string
     */
    public $date;
    /**
     * @Column(type="string", name="front_img")
     * @var string
     */
    public $front_img;
    /**
     * @Column(type="string", name="tags", nullable=true)
     * @var int
     */
    public $tags;
    /**
     * @Column(type="float", name="rate", nullable=true)
     * @var float
     */
    public $rate;


    public function __construct()
    {
        $this->title = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getCategory()
    {
        return $this->category;
    }
    public function getTags()
    {
        return $this->tags;
    }
    public function getRate()
    {
        return $this->rate;
    }
    public function setId($id)
    {
        $this->id = $id;
    }
    public function setTitle($title)
    {
        $this->title = $title;
    }
    public function setCategory($category)
    {
        $this->category = $category;
    }
    public function setDescription($description)
    {
        $this->description = $description;
    }
    public function setText($text)
    {
        $this->text = $text;
    }
    public function setDate($date)
    {
        $this->date = $date;
    }
    public function setFrontImg($front_img)
    {
        $this->front_img = $front_img;
    }
    public function setTags($tags)
    {
        $this->tags = $tags;
    }
    public function setRate($rate)
    {
        $this->rate = $rate;
    }
}