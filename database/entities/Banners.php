<?php

namespace Database\Entities;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="banners")
 *
 **/

class Banners
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @Column(type="string", name="title", length=100)
     * @var string
     */
    public $title;

    /**
     * @Column(type="string", name="image", length=1000)
     * @var string
     */
    public $image;

    /**
     * @Column(type="string", name="link", length=1000)
     * @var string
     */
    public $link;

    /**
     * @Column(type="string", name="text", length=1000)
     * @var string
     */
    public $text;

    /**
     * @Column(type="date", name="date_start", length=100)
     * @var string
     */
    public $date_start;

    /**
     * @Column(type="date", name="date_end", length=100)
     * @var string
     */
    public $date_end;

    public function getId()
    {
        return $this->id;
    }
    public function getTitle()
    {
        return $this->title;
    }
    public function getImage()
    {
        return $this->image;
    }
    public function getLink()
    {
        return $this->link;
    }
    public function getText()
    {
        return $this->text;
    }
    public function getDateStart()
    {
        return $this->date_start;
    }
    public function getDateEnd()
    {
        return $this->date_end;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setTitle($title)
    {
        $this->title = $title;
    }
    public function setImage($image)
    {
        $this->image = $image;
    }
    public function setLink($link)
    {
        $this->link = $link;
    }
    public function setText($text)
    {
        $this->text = $text;
    }
    public function setDateStart($date_start)
    {
        $this->date_start = $date_start;
    }
    public function setDateEnd($date_end)
    {
        $this->date_end = $date_end;
    }
}