<?php

namespace Database\Entities;

use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="users")
 *
 **/

class Users
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    public $id;
    /**
     * @Column(type="string", name="login")
     * @OneToMany(targetEntity="CommentsNews", mappedBy="user")
     * @OneToMany(targetEntity="Rates", mappedBy="user")
     */
    public $login;
    /**
     * @Column(type="string", name="password")
     * @var string
     */
    protected $password;
    /**
     * @Column(type="string", name="passwordr")
     * @var string
     */
    protected $password_r;
    /**
     * @Column(type="string", name="email")
     * @var string
     */
    protected $email;
    /**
     * @Column(type="string", name="firstname")
     * @var string
     */
    protected $first_name;
    /**
     * @Column(type="string", name="lastname")
     * @var string
     */
    protected $last_name;
    /**
     * @Column(type="string", name="photo", nullable=true)
     * @var string
     */
    public $photo;
    /**
     * @Column(type="string", name="site")
     * @var string
     */
    protected $site;
    /**
     * @Column(type="integer", name="role", nullable=true)
     * @var integer
     */
    protected $role;

    public function __construct()
    {
        $this->login = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getPasswordR()
    {
        return $this->password_r;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function getSite()
    {
        return $this->site;
    }

    public function getUserRole()
    {
        return $this->role;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setLogin($login)
    {
        $this->login = $login;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }
    public function setPasswordR($password_r)
    {
        $this->password_r = $password_r;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }
    public function setSite($site)
    {
        $this->site = $site;
    }
    public function setUserRole($role)
    {
        $this->role = $role;
    }

}