<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 05.01.2017
 * Time: 11:39
 */

namespace Database\Entities;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="footer")
 *
 **/

class Footer
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    public $id;
    /**
     * @Column(type="string", name="text", length=1000)
     * @var string
     */
    protected $text;
    /**
     * @Column(type="string", name="link", length=1000)
     * @var string
     */
    protected $link;

    public function getLink()
    {
        return $this->link;
    }
    public function getText()
    {
        return $this->text;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setLink($link)
    {
        $this->link = $link;
    }
    public function setText($text)
    {
        $this->text = $text;
    }
}