<?php

namespace Database\Entities;

use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="rates")
 *
 **/
class Rates
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    public $id;
    /**
     * @ManyToOne(targetEntity="UsersAPIs", inversedBy="id")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;
    /**
     * @ManyToOne(targetEntity="News", inversedBy="id")
     * @JoinColumn(name="post_id", referencedColumnName="id")
     */
    public $post;
    /**
     * @Column(type="integer", name="rate")
     */
    public $rate;

    public function getId()
    {
        return $this->id;
    }
    public function getUser()
    {
        return $this->user;
    }
    public function getPost()
    {
        return $this->post;
    }
    public function getRate()
    {
        return $this->rate;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setUser($user)
    {
        $this->user = $user;
    }
    public function setPost($post)
    {
        $this->post = $post;
    }
    public function setRate($rate)
    {
        $this->rate = $rate;
    }
}