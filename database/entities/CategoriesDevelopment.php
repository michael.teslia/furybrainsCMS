<?php

namespace Database\Entities;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="categories_development")
 *
 **/

class CategoriesDevelopment
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     *
     */
    public $id;
    /**
     * @var string
     * @OneToMany(targetEntity="Development", mappedBy="category")
     * @Column(type="string", name="title")
     */
    public $title;

    public function __construct()
    {
        $this->title = new ArrayCollection();
    }

    public function getId()
    {
        return $this->title->toArray();
    }

}