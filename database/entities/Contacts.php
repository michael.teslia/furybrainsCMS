<?php

namespace Database\Entities;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="contacts")
 *
 **/

class Contacts
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     *
     */
    public $id;
}