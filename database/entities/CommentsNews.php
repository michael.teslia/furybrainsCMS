<?php

namespace Database\Entities;

use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="comments_news")
 *
 **/
class CommentsNews
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    public $id;
    /**
     * @ManyToOne(targetEntity="UsersAPIs", inversedBy="id")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;
    /**
     * @ManyToOne(targetEntity="News", inversedBy="id")
     * @JoinColumn(name="post_id", referencedColumnName="id")
     */
    public $post;
    /**
     * @Column(type="string", name="text")
     */
    public $text;
    /**
     * @Column(type="string", name="date")
     */
    public $date;

    public function getId()
    {
        return $this->id;
    }
    public function getUser()
    {
        return $this->user;
    }
    public function getPost()
    {
        return $this->post;
    }
    public function getText()
    {
        return $this->text;
    }
    public function getDate()
    {
        return $this->date;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setUser($user)
    {
        $this->user = $user;
    }
    public function setPost($post)
    {
        $this->post = $post;
    }
    public function setText($text)
    {
        $this->text = $text;
    }
    public function setDate($date)
    {
        $this->date = $date;
    }
}