<?php

namespace Database\Entities;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="about_us")
 *
 **/

class AboutUs
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     *
     */
    public $id;
}