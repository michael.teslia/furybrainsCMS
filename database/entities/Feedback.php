<?php

namespace Database\Entities;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="feedback")
 *
 **/

class Feedback
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @Column(type="string", name="name", length=100)
     * @var string
     */
    public $name;

    /**
     * @Column(type="string", name="email", length=100)
     * @var string
     */
    public $email;

    /**
     * @Column(type="string", name="subject", length=1000)
     * @var string
     */
    public $subject;

    /**
     * @Column(type="string", name="message", length=10000)
     * @var string
     */
    public $message;

    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getSubject()
    {
        return $this->subject;
    }
    public function getMessage()
    {
        return $this->message;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
    public function setMessage($message)
    {
        $this->message = $message;
    }
}