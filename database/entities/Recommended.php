<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 05.01.2017
 * Time: 19:26
 */

namespace Database\Entities;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="recommended")
 *
 **/

class Recommended
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @Column(type="string", name="title", length=100)
     * @var string
     */
    public $title;

    /**
     * @Column(type="string", name="image", length=1000)
     * @var string
     */
    public $image;

    /**
     * @Column(type="string", name="link", length=1000)
     * @var string
     */
    public $link;

    /**
     * @Column(type="integer", name="position", length=10)
     * @var int
     */
    public $position;

    public function getId()
    {
        return $this->id;
    }
    public function getTitle()
    {
        return $this->title;
    }
    public function getImage()
    {
        return $this->image;
    }
    public function getLink()
    {
        return $this->link;
    }
    public function getPosition()
    {
        return $this->position;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setTitle($title)
    {
        $this->title = $title;
    }
    public function setImage($image)
    {
        $this->image = $image;
    }
    public function setLink($link)
    {
        $this->link = $link;
    }
    public function setPosition($position)
    {
        $this->position = $position;
    }
}