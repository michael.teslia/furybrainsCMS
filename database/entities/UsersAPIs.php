<?php

namespace Database\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @Entity @Table(name="users_apis")
 *
 **/

class UsersAPIs
{
    /**
     * @Id
     * @Column(type="string", name="id", length=255)
     * @OneToMany(targetEntity="CommentsNews", mappedBy="user")
     * @OneToMany(targetEntity="Rates", mappedBy="user")
     */
    public $id;
    /**
     * @Column(type="string", name="firstname")
     * @var string
     */
    public $first_name;
    /**
     * @Column(type="string", name="lastname")
     * @var string
     */
    public $last_name;
    /**
     * @Column(type="string", name="photo", nullable=true)
     * @var string
     */
    public $photo;
    /**
     * @Column(type="string", name="email", nullable=true)
     * @var string
     */
    public $email;
    /**
     * @Column(type="string", name="security_key", nullable=true)
     * @var string
     */
    protected $security_key;
    /**
     * @Column(type="string", name="locale", nullable=true)
     * @var string
     */
    protected $locale;
    /**
     * @Column(type="string", name="link", nullable=true)
     * @var string
     */
    protected $link;
    /**
     * @Column(type="string", name="authtype")
     * @var string
     */
    protected $auth_type;

    public function __construct()
    {
        $this->id = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }
    public function getFirstName()
    {
        return $this->first_name;
    }
    public function getLastName()
    {
        return $this->last_name;
    }
    public function getPhoto()
    {
        return $this->photo;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getSecurityKey()
    {
        return $this->security_key;
    }
    public function getLocale()
    {
        return $this->locale;
    }
    public function getLink()
    {
        return $this->link;
    }
    public function getAuthType()
    {
        return $this->auth_type;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function setSecurityKey($security_key)
    {
        $this->security_key = $security_key;
    }
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }
    public function setLink($link)
    {
        $this->link = $link;
    }
    public function setAuthType($auth_type)
    {
        $this->auth_type = $auth_type;
    }
}