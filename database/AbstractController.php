<?php
/**
 * Created by PhpStorm.
 * User: mikzt
 * Date: 10.02.2017
 * Time: 18:58
 */

namespace FuryBrains\Controller;


class AbstractController
{
    public function replaceBBCode($text_post) {
        $str_search = array(
            "#\\\n#is",
            "#\[b\](.*)\[\/b\]#isU",
            "#\[h1\](.*)\[\/h1\]#isU",
            "#\[h2\](.*)\[\/h2\]#isU",
            "#\[h3\](.*)\[\/h3\]#isU",
            "#\[h4\](.*)\[\/h4\]#isU",
            "#\[h5\](.*)\[\/h5\]#isU",
            "#\[h6\](.*)\[\/h6\]#isU",
            "#\[i\](.*)\[\/i\]#isU",
            "#\[u\](.*)\[\/u\]#isU",
            "#\[youtube\](.*)\[\/youtube\]#isU",
            "#\[code\](.*)\[\/code\]#isU",
            "#\[quote\](.*)\[\/quote\]#isU",
            "#\[url=(.*)\](.*)\[\/url\]#isU",
            "#\[url\](.*)\[\/url\]#isU",
            "#\[img\](.*)\[\/img\]#isU",
            "#\[img=(.*)\](.*)\[\/img\]#isU",
            "#\[size=(.+?)\](.+?)\[\/size\]#is",
            "#\[color=(.+?)\](.+?)\[\/color\]#is",
            "#\[list\](.+?)\[\/list\]#is",
            "#\[listn](.+?)\[\/listn\]#is",
            "#\[\*\](.+?)\[\/\*\]#"
        );
        $str_replace = array(
            "<br />",
            "<b>\\1</b>",
            "<h1>\\1</h1>",
            "<h2>\\1</h2>",
            "<h3>\\1</h3>",
            "<h4>\\1</h4>",
            "<h5>\\1</h5>",
            "<h6>\\1</h6>",
            "<i>\\1</i>",
            "<span style='text-decoration:underline'>\\1</span>",
            "<div class=\"embed-responsive embed-responsive-16by9\">
                <iframe class=\"embed-responsive-item\" src=\"\\1\"></iframe>
                </div>",
            "<code class='code'>\\1</code>",
            "<table width = '100%'>
                <tr class='alt'>
                    <td class='quote'>
                        <img src='img/opened_quote.jpg' width='25px' style='float: left'>
                    </td>
                </tr>
                <tr class='alt'>
                    <td class='quote' valign=''>
                       \\1
                    </td>
                </tr>
                <tr class='alt'>
                    <td class='quote'>
                        <img src='img/closed_quote.jpg' width='25px' style='float: right'>
                    </td>
                </tr>
            </table>",
            "<a href='\\1'>\\2</a>",
            "<a href='\\1'>\\1</a>",
            "<img src='\\1'/>",
            "<img alt ='\\1 — FURYbrains' src='\\2'/>",
            "<span style='font-size:\\1%'>\\2</span>",
            "<span style='color:\\1'>\\2</span>",
            "<ul>\\1</ul>",
            "<ol>\\1</ol>",
            "<li>\\1</li>"
        );
        return preg_replace($str_search, $str_replace, $text_post);
    }


    function imageresize($newWidth, $targetFile, $originalFile) {

        $info = getimagesize($originalFile);
        $mime = $info['mime'];

        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw new Exception('Unknown image type.');
        }

        $img = $image_create_func($originalFile);
        list($width, $height) = getimagesize($originalFile);

        $newHeight = ($height / $width) * $newWidth;
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        if (file_exists($targetFile)) {
            unlink($targetFile);
        }
        $image_save_func($tmp, "$targetFile");
    }

    function file_get_contents_curl($url, $params) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        $result = curl_exec($curl);
        curl_close($curl);

        var_dump($result);

        return $result;
    }

}